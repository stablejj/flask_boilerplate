#!venv/bin/python

import imp

from migrate.versioning import api
from app import DB, CONFIG_CACHE


SQLALCHEMY_DATABASE_URI = CONFIG_CACHE['SQLALCHEMY_DATABASE_URI']
SQLALCHEMY_MIGRATE_REPO = CONFIG_CACHE['SQLALCHEMY_MIGRATE_REPO']

DB_VERSION = api.db_version(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
MIGRATION = SQLALCHEMY_MIGRATE_REPO + ('/versions/%03d_migration.py' % (DB_VERSION + 1))
TMP_MODULE = imp.new_module('old_model')
OLD_MODULE = api.create_model(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)

#pylint: disable=exec-used
exec(OLD_MODULE, TMP_MODULE.__dict__)

SCRIPT = api.make_update_script_for_model(
	   SQLALCHEMY_DATABASE_URI,
	   SQLALCHEMY_MIGRATE_REPO,
	   TMP_MODULE.meta, #TODO: Check implementation
	   DB.metadata)
open(MIGRATION, "wt").write(SCRIPT)

api.upgrade(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
DB_VERSION = api.db_version(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)

print('New migration saved as ' + MIGRATION)
print('Current database version: ' + str(DB_VERSION))
