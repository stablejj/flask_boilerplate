'''
DB HELPERS
==========

While not mandatory, you can define queries for your views here.

Example:

def get_post_list():
    return models.Post.query.all()
'''
