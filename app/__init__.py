import os

from functools import wraps
from flask import Flask, flash, session, wrappers, url_for, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import Form


# ===== Section Config ===== #
APP = Flask(__name__)

CONFIG_CACHE = {}
CONFIG_PATH = os.path.join(APP.root_path, 'config_user.py')

print('Detecting ' + CONFIG_PATH)

if os.path.isfile(CONFIG_PATH):
    print("User Config detected... using that.")
    APP.config.from_pyfile('config_user.py')
    CONFIG_CACHE = APP.config.copy()

else:
    print("No user config detected... using default config...")
    APP.config.from_pyfile('config.py')
    CONFIG_CACHE = APP.config.copy()

# ===== Section Connect DB===== #
DB = SQLAlchemy(APP)

# ===== App init ===== #
#pylint: disable=wrong-import-position
from app import views, models, dbhelpers
