#!venv/bin/python

#pylint: disable=unused-import
import db_create # Creates database if not exists

from app import APP, CONFIG_CACHE


def main():
    ssl = False
    if 'SSL' in CONFIG_CACHE:
        if CONFIG_CACHE['SSL']:
            print('establishing https')
            ssl = True
        else:
            print('establishing http')
    if ssl:
        APP.run(debug=True, ssl_context='adhoc')
    else:
        APP.run(debug=True)


if __name__ == '__main__':
    main()
