from app import DB
'''
Models
==============================================================
All SQL Alchemy Model and Table Definitions go here.
Any change you make to this File will be reflected to your
Database when running db_migrate.py in your Project Root


Examples:


A Simple User Table:
==============================================================
class User(DB.Model):
	__tablename__ = 'users'
    id = DB.Column(DB.Integer, primary_key=True)
    name = DB.Column(DB.String(80), unique=True)
    last_login = DB.Column(DB.DateTime())

    def __repr__(self):
        return '<user %r>' % (self.name)
===============================================================

A (blog)post associated to one user
===============================================================
class Post(DB.Model):
	__tablename__ = 'posts'
    id = DB.Column(DB.Integer, primary_key=True, )
    title = DB.Column(DB.String(40))
    body = DB.Column(DB.String())
    created_at = DB.Column(DB.DateTime())
    user_id = DB.Column(DB.Integer, DB.ForeignKey('user.id'))
    tags = DB.Column(DB.String())

    user = DB.relationship('User', foreign_keys=user_id)
===============================================================

A Many To Many Relationship helper Table
===============================================================
friend_identifier = DB.Table('friend_identifier',
    DB.Column('user_id', DB.Integer, DB.ForeignKey('users.id')),
    DB.Column('user_id', DB.Integer, DB.ForeignKey('users.id'))
)
===============================================================
'''