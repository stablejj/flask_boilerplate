'''
Views
=====
'''

from flask import render_template  # , flash, redirect, url_for, request, session
from app import APP, models, dbhelpers


# ==== Section Routes ==== #
@APP.route('/')
@APP.route('/index')
def index():
    '''renders index page'''
    return render_template(
        'index.html'
    )
