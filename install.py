#!venv/bin/python

'''Installs BOOTSTRAP Node Dependencies'''

import os
import subprocess


PREV_PATH = os.getcwd()
PATH = os.path.join(os.path.dirname(__file__), 'app/static/')

os.chdir(PATH)
subprocess.run(["npm", "install"])
os.chdir(PREV_PATH)

print("All Done")
