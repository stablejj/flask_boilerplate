#!venv/bin/python

import os

from migrate.versioning import api
from app import DB, CONFIG_CACHE

DB.create_all()

if not os.path.exists(CONFIG_CACHE['SQLALCHEMY_MIGRATE_REPO']):
    api.create(CONFIG_CACHE['SQLALCHEMY_MIGRATE_REPO'], 'database repository')
    api.version_control(CONFIG_CACHE['SQLALCHEMY_DATABASE_URI'],
                        CONFIG_CACHE['SQLALCHEMY_MIGRATE_REPO'])
else:
    try:
        api.version_control(CONFIG_CACHE['SQLALCHEMY_DATABASE_URI'],
                            CONFIG_CACHE['SQLALCHEMY_MIGRATE_REPO'],
                            api.version(CONFIG_CACHE['SQLALCHEMY_MIGRATE_REPO']))
    except api.exceptions.DatabaseAlreadyControlledError as error:
        pass
